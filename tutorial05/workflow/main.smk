configfile: 'config/config.yaml'
SAMPLES = ['A', 'B']


rule all:
    input:
        config['wdir'] + config['results'] + 'out.vcf'


rule merge_reads:
    input:
        expand(config['wdir'] + config['resources'] + '{sample}.txt', sample=config['samples'])
    output:
        config['wdir'] + config['resources'] + 'reads.txt'
    shell:
        'cat {input} >> {output}'


rule create_index:
    input:
        config['wdir'] + config['resources'] + 'reference.fa'
    benchmark:
        "benchmarks/index.benchmark.txt"
    log:
        "logs/index.log"
    threads: 4
    output:
        touch(config['wdir'] + config['results'] + 'bwa_index.done')
    shell:
        '(bwa index {input}) 2> {log}'


rule allign_reads_1:
    input:
        check = config['wdir'] + config['results'] + 'bwa_index.done',
        ref = config['wdir'] + config['resources'] + 'reference.fa',
        reads = config['wdir'] + config['resources'] + 'reads.txt'
    log:
        "logs/allign1.log"
    benchmark:
        "benchmarks/allign1.benchmark.txt"
    threads: 8
    output:
        temp('out.sai')
    shell:
        '(bwa aln -I -t {threads} {input.ref} {input.reads} > {output}) 2> {log}'


rule allign_reads_2:
    input:
        ref = config['wdir'] + config['resources'] + 'reference.fa',
        sai = 'out.sai',
        reads = config['wdir'] + config['resources'] + 'reads.txt'
    benchmark:
        "benchmarks/allign2.benchmark.txt"
    log:
        "logs/allign2.log"
    output:
        config['wdir'] + config['results'] + 'alligned/out.sam'
    shell:
        '(bwa samse {input.ref} {input.sai} {input.reads} > {output}) 2> {log}'


rule convert_and_sort:
    input:
        config['wdir'] + config['results'] + 'alligned/out.sam'
    benchmark:
        "benchmarks/convert.sort.benchmark.txt"
    log:
        "logs/convert.sort.log"
    output: 
        config['wdir'] + config['results'] + 'temp/out.sorted.bam'
    shell:
        '(samtools view -S -b {input} | samtools sort -o {output}) 2> {log}'


rule detect_delete_duplicates:
    input:
        config['wdir'] + config['results'] + 'temp/out.sorted.bam'
    output:
        config['wdir'] + config['results'] + 'filtered/out.dedupe.bam'
    benchmark:
        "benchmarks/duplicate.benchmark.txt"
    threads: 2
    log:
        "logs/duplicate.log"
    shell:
        '''(java -jar picard/build/libs/picard.jar MarkDuplicates \
                            MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000\
                            METRICS_FILE=out.metrics \
                            REMOVE_DUPLICATES=true \
                            ASSUME_SORTED=true  \
                            VALIDATION_STRINGENCY=LENIENT \
                            INPUT={input} \
                            OUTPUT={output}) 2> {log}'''


rule index_results:
    input:
        config['wdir'] + config['results'] + 'filtered/out.dedupe.bam'
    benchmark:
        "benchmarks/sam.index.benchmark.txt"
    log:
        "logs/sam.index.log"
    output:
        touch(config['wdir'] + config['results'] + 'sam_index.done')
    shell:
        '(samtools index {input}) 2> {log}'

rule pileup_and_convert:
    input:
        ref = config['wdir'] + config['resources'] + 'reference.fa',
        filtered = config['wdir'] + config['results'] + 'filtered/out.dedupe.bam'
    benchmark:
        "benchmarks/pileup.benchmark.txt"
    log:
        "logs/pileup.log"
    threads: 2
    output:
        config['wdir'] + config['results'] + 'out.vcf'
    shell:
        '(samtools mpileup -uf {input.ref} {input.filtered} | bcftools view -> {output}) 2> {log}'