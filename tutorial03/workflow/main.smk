configfile: "config/config.yaml"

rule all:
    input:
        config["wdir"] + config["results"] + "test.txt",
        config["wdir"] + config["results"] + "test.fasta",
        config["wdir"] + config["results"] + "merged.bam"

include: config["wdir"] + config["rules"] + "http" + config["smk"]
include: config["wdir"] + config["rules"] + "ncbi" + config["smk"]
include: config["wdir"] + config["rules"] + "merge_files"+ config["smk"]

