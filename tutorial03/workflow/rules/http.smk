rule move_file:
    input:
        "test.txt"
    output:
        config["wdir"] + config["results"] + "test.txt"
    shell:
        "mv {input} {output}"

rule obtain_file:
    output: 
        "test.txt"
    shell:
        "wget " + config["url"] + "test.txt"
        