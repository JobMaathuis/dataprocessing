from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider
NCBI = NCBIRemoteProvider(email="j.maathuis@st.hanze.nl") # email required by NCBI to prevent abuse

rule obtain_fasta:
    input:
        NCBI.remote("KY785484.1.fasta", db="nuccore")
    output:
        config["wdir"] + config["results"] + "test.fasta"
    shell:
        "mv {input} {output}"