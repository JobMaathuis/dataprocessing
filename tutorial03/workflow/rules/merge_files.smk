SAMPLES = [x for x in 'ABDEFGHIJ']

rule merge:
    input:
       expand("{sample}.bam", sample=SAMPLES)
    output:
        config["wdir"] + config["results"] + "merged.bam"
    shell:
        "cat {input} >> {output}"


rule download:
    output:
       temp("{sample}.bam")
    shell:
        "wget " + config["url"] + config["wc03"] + "{output}"