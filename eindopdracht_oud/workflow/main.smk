configfile: "config/config.yaml"

rule all:
    input:
        'all.done'

include: config['wdir'] + config['rules'] + 'map_genes' + config['smk']
include: config['wdir'] + config['rules'] + 'create_features' + config['smk']
include: config['wdir'] + config['rules'] + 'build_sqn' + config['smk']
