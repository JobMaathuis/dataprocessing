configfile: "config/config.yaml"

rule all:
    input:
        config['wdir'] + config['resources'] + config['organism'] + '_assembly.fas'
    

rule index:
    input: 
        genome = config['wdir'] + config['resources'] + config['organism'] + '.fas'
    log:
        'logs/index.log'
    output:
        touch('check.txt'),
        'index_genome.1.bt2',
        'index_genome.2.bt2',
        'index_genome.3.bt2',
        'index_genome.4.bt2',
        'index_genome.rev.1.bt2',
        'index_genome.rev.2.bt2'
    shell:
        '(bowtie2-build {input.genome} index_genome) 2> {log}'


rule mapping:
    input:
        check = 'check.txt',
        fasta = config['wdir'] + config['resources'] + config['organism'] + '_genes_mitos2.fas'
    # log:
    #     'logs/mapping.log'
    output:
        config['wdir'] + config['resources'] + 'out.sam'
    shell:
        'bowtie2 -x index_genome -f {input.fasta} -S {output}'


rule sort_sam:
    input:
        config['wdir'] + config['resources'] + 'out.sam'
    output:
        config['wdir'] + config['resources'] + 'sorted.sam'
    shell:
        '''java -jar resources/picard/build/libs/picard.jar SortSam \
                I= {input} \
                O= {output} \
                SORT_ORDER=coordinate'''

rule convert_to_fasata:
    input:
        sam = config['wdir'] + config['resources'] + 'sorted.sam',
        genome = config['wdir'] + config['resources'] + config['organism'] + '.fas'
    output:
        config['wdir'] + config['resources'] + config['organism'] + '_assembly.fas'
    shell:
        'resources/scripts/sam2fasta.py {input.genome} {input.sam} {output}'