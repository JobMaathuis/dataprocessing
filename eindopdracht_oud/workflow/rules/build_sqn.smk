rule build_sqn_file:
    input:
        genome = config['wdir'] + config['resources'] + config['organism'] + '.fas',
        features = config['wdir'] + config['result'] + config['organism'] + '_feature_table.tbl',
        template = config['wdir'] + config['resources'] + 'submission_template.sbt'
    output:
        val = config['wdir'] + config['resources'] + config['organism'] + '.val',
        sqn = config['wdir'] + config['resources'] + config['organism'] + '.sqn',
        gbf = config['wdir'] + config['resources'] + config['organism'] + '.gbf',
        error = config['wdir'] + config['resources'] + 'errorsummary.val',
    shell:
        'tbl2asn -i {input.genome} -f {input.features} -t {input.template} -a s -V bv -T -j "[mgcode=5] [location=mitochondrion] [organism=Hyalella solida]"'


rule move_files:
    input:
        val = config['wdir'] + config['resources'] + config['organism'] + '.val',
        sqn = config['wdir'] + config['resources'] + config['organism'] + '.sqn',
        gbf = config['wdir'] + config['resources'] + config['organism'] + '.gbf',
        error = config['wdir'] + config['resources'] + 'errorsummary.val',
        output_path = config['wdir'] + config['result']
    output:
        touch('all.done')
    shell:
        'mv {input.val} {input.output_path}; mv {input.sqn} {input.output_path}; mv {input.gbf} {input.output_path}; mv {input.error} {input.output_path}'