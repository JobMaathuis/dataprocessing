rule map_genes:
    input:
        genome = config['wdir'] + config['resources'] + config['organism'] + '.fas',
        genes = config['wdir'] + config['resources'] + config['organism'] + '_genes_mitos2.fas'
    log:
        'logs/map_genes.log'
    output:
        config['wdir'] + config['resources'] + config['organism'] + '_assembly.fas'
    shell:
        '(resources/scripts/mitos2fasta.py -m {input.genome} -g {input.genes} -c Y > {output}) 2> {log}'


# ./mitos2fasta.py -m ./example/input/Hyalella_solida_mitogenome.fas -g ./example/input/Hyalella_solida_genes_mitos2.fas -c Y > ./example/input/Hyalella_solida_assembly.fas