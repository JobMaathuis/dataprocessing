rule create_feature_table:
    input:
        assembly = config['wdir'] + config['resources'] + config['organism'] + '_assembly.fas',
        genes = config['wdir'] + config['resources'] + 'forward_genes.txt',
    output:
        config['wdir'] + config['result'] + config['organism'] + '_feature_table.tbl'
    shell:
        'resources/scripts/aln2tbl.py -f {input.assembly} -g {input.genes} -c 5 > {output}'