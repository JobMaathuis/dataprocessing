rule all:
    input:
        '/homes/jmaathuis/Documents/dataprocessing/tutorial04/images/heatmap.jpg'

rule make_heatmap:
    input:
        '/commons/Themas/Thema11/Dataprocessing/WC04/data/gene_ex.csv'
    output:
        '/homes/jmaathuis/Documents/dataprocessing/tutorial04/images/heatmap.jpg'
    shell:
        'Rscript resources/make_heatmap.R {input} {output}'
    