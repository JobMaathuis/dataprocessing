configfile: 'config/config.yaml'
SAMPLES = ['A', 'B']

rule all:
    input:
        config['wdir'] + config['results'] + 'out.vcf'

rule merge_reads:
    input:
        expand(config['wdir'] + config['resources'] + '{sample}.txt', sample=config['samples'])
    output:
        config['wdir'] + config['resources'] + 'reads.txt'
    shell:
        'cat {input} >> {output}'

rule create_index:
    input:
        config['wdir'] + config['resources'] + 'reference.fa'
    output:
        touch(config['wdir'] + config['results'] + 'bwa_index.done')
    shell:
        'bwa index {input}'

rule allign_reads_1:
    input:
        check = config['wdir'] + config['results'] + 'bwa_index.done',
        ref = config['wdir'] + config['resources'] + 'reference.fa',
        reads = config['wdir'] + config['resources'] + 'reads.txt'
    output:
        temp('out.sai')
    shell:
        'bwa aln -I -t 8 {input.ref} {input.reads} > {output}'

rule allign_reads_2:
    input:
        ref = config['wdir'] + config['resources'] + 'reference.fa',
        sai = 'out.sai',
        reads = config['wdir'] + config['resources'] + 'reads.txt'
    output:
        config['wdir'] + config['results'] + 'alligned/out.sam'
    shell:
        'bwa samse {input.ref} {input.sai} {input.reads} > {output}'

rule convert_and_sort:
    input:
        config['wdir'] + config['results'] + 'alligned/out.sam'
    output: 
        config['wdir'] + config['results'] + 'temp/out.sorted.bam'
    shell:
        'samtools view -S -b {input} | samtools sort -o {output}'

rule detect_delete_duplicates:
    input:
        config['wdir'] + config['results'] + 'temp/out.sorted.bam'
    output:
        config['wdir'] + config['results'] + 'filtered/out.dedupe.bam'
    shell:
        '''java -jar picard/build/libs/picard.jar MarkDuplicates \
                            MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000\
                            METRICS_FILE=out.metrics \
                            REMOVE_DUPLICATES=true \
                            ASSUME_SORTED=true  \
                            VALIDATION_STRINGENCY=LENIENT \
                            INPUT={input} \
                            OUTPUT={output}'''

rule index_results:
    input:
        config['wdir'] + config['results'] + 'filtered/out.dedupe.bam'
    output:
        touch(config['wdir'] + config['results'] + 'sam_index.done')
    shell:
        'samtools index {input}'

rule pileup_and_convert:
    input:
        ref = config['wdir'] + config['resources'] + 'reference.fa',
        filtered = config['wdir'] + config['results'] + 'filtered/out.dedupe.bam'
    output:
        config['wdir'] + config['results'] + 'out.vcf'
    shell:
        'samtools mpileup -uf {input.ref} {input.filtered} | bcftools view -> {output}'