# -*- python -*-
configfile: "config/config.yaml"

SAMPLES = ["A", "B", "C"]

rule all:
    message: "rule all is being executed"
    input:
        "results/out.html"

rule bwa_map:
    input:
        config["data"] + config["genome"],
        config["data"] + config["samples"] + "{sample}.fastq"
    benchmark:
        "benchmarks/{sample}.bwa.benchmark.txt"
    output:
        "results/mapped_reads/{sample}.bam"
    message: "executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"

rule samtools_sort:
    input:
        "results/mapped_reads/{sample}.bam"
    output:
        "results/sorted_reads/{sample}.bam"
    message: "sorting the {input} to generate {output}"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"

rule samtools_index:
    input:
        "results/{sample}.bam"
    output:
        "results/{sample}.bam.bai"
    message: "indexing the {input} to generate {output}"
    shell:
        "samtools index {input}"

rule bcftools_call:
    input:
        fa=config["data"] + config["genome"],
        bam=expand("results/sorted_reads/{sample}.bam", sample=SAMPLES),
        bai=expand("results/sorted_reads/{sample}.bam.bai", sample=SAMPLES)
    output:
        "results/calls/all.vcf"
    message: "calling the variants of {input} to generate {output}"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"

rule report:
    input:
        "results/calls/all.vcf"
    output:
        html = "results/out.html"
    message: "writing report..."
    run:
        from snakemake.utils import report
        n_calls = 0
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))
        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeas reference genome 
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants
        """, output.html, metadata="Author: Job Maathuis")

rule clean:
    shell:
        'rm -R results/*'
      